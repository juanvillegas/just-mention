jQuery(document).ready(function($) {

    var $bbpress_reply = $(".bbp-reply-form, #wp-bbp_topic_content-wrap");

    $bbpress_reply.find("textarea").mentionsInput({
        showAvatars: false,
        onDataRequest: function (mode, query, callback) {
            var $request,
                $this = this;

            $request = $.ajax({
                url: jm_data.ajax,
                data: {
                    action: "jm_data_request",
                    query: query
                },
                dataType: "json",
                accepts: "json",
                method: "post"
            });

            $request.success(function(response) {
                if (response.result) {
                    callback.call(this, response.data.matches);
                }
            });
        }
    });
});