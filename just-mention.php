<?php
/*
Plugin Name: Just Mention
Description: Simple: add mentions to Wordpress.
Version: 1.0.2
Author: Juan Villegas
Author URI: http://teamhandstand.com
Text Domain: just-mention
Domain Path: /languages
*/


define('JUST_MENTION_URL', plugins_url('', __FILE__));
define('JUST_MENTION_PATH', plugin_dir_path(__FILE__));

require_once JUST_MENTION_PATH . '/app/JMSearchService.php';
require_once JUST_MENTION_PATH . '/app/JMNotifyService.php';
require_once JUST_MENTION_PATH . '/app/JMContentFilter.php';

/*
 * Enqueue the tagging script only for:
 * - single custom post type topic || reply
 */
//add_action('admin_enqueue_scripts', function($hook) {
//	$screen = get_current_screen();
//
//	if (($hook == 'post-new.php') && $screen->post_type == 'topic') {
//		jm_enqueue();
//	}
//});

add_action('wp_enqueue_scripts', function() {
	if (is_singular(array('topic', 'reply', 'forum'))) {
		jm_enqueue();
	}
});

function jm_enqueue() {
	// dependencies
	wp_enqueue_script('jquery');
	wp_enqueue_script('underscore');

	wp_enqueue_style('jquery_mentions_input_css', JUST_MENTION_URL . '/public/js/jquery-mentions-input/jquery.mentionsInput.css');
	wp_enqueue_script('jquery_mentions_input_lib', JUST_MENTION_URL . '/public/js/jquery-mentions-input/lib/jquery.elastic.js', array('jquery', 'underscore'));
	wp_enqueue_script('jquery_mentions_input_js', JUST_MENTION_URL . '/public/js/jquery-mentions-input/jquery.mentionsInput.js', array('jquery', 'underscore'));

	// custom scripts
	wp_enqueue_script('just-mention-js', JUST_MENTION_URL . '/public/js/just-mention.js', array('jquery'));
	wp_localize_script('just-mention-js', 'jm_data', array('ajax' => admin_url('admin-ajax.php')));

	wp_enqueue_style('jm_styles', JUST_MENTION_URL . '/public/css/front.css');
}

add_action('wp_ajax_nopriv_jm_data_request', 'jm_handle_data_request');
add_action('wp_ajax_jm_data_request', 'jm_handle_data_request');
function jm_handle_data_request() {
	$search_service = new JMSearchService();
	$users = $search_service->search($_POST['query']);
	$results = array();

	foreach ($users as $user) {
		$user_data = array(
			'id' => $user->ID,
			'name' => $user->display_name,
			'username' => $user->user_login,
			'type' => 'contact'
		);

		if (get_option('show_avatars')) {
			$user_data['avatar'] = get_avatar_url(get_avatar($user->ID, 32));
			$user_data['avatar_img'] = get_avatar($user->ID, 32);
		}

		$results[] = $user_data;
	}

	echo json_encode(array(
		'result' => true,
		'data' => array(
			'matches' => $results
		)
	));
	exit;
}


add_filter('bbp_get_reply_content', function($content, $reply_id){
	$filter = new JMContentFilter($content);

	return $filter->filter();
}, 10, 2);

/*
 * When a Reply is saved for the first time, add all the @mentions found
 */
add_action('save_post', function ($post_id) {
	if (wp_is_post_revision($post_id)) {
		return;
	}

	$post = get_post($post_id);

	if (! $post) {
		return;
	}

	if (! in_array($post->post_type, array('reply', 'topic'))) {
		return;
	}

	$service = new JMNotifyService($post);

	$service->handle_save();
});

/*
 * Helpers
 */
if (! function_exists('get_avatar_url')):
	function get_avatar_url($avatar_img_tag){
		if ($avatar_img_tag) {
			preg_match("/src='(.*?)'/i", $avatar_img_tag, $matches);
			return $matches[1];
		} else {
			return '';
		}
	}
endif;
