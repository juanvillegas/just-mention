<?php

class JMSearchService {

	const MAX_PER_PAGE = 10;

	public function search($query, $args = array()) {
		$users = get_users(array(
			'search' => $query . '*',
			'number' => isset($args['number']) ? $args['number'] : self::MAX_PER_PAGE
		));

		return $users;
	}

}