<?php

class JMNotifyService {

	/**
	 * @var \WP_Post
	 */
	private $post;

	function __construct($post) {
		$this->post = $post;
	}

	public function handle_save() {
		// get all mentions in the post_content
		$mentions = $this->get_mentions();
		if ($mentions === false) {
			return false;
		}

		// get the WP_Users from the array of mentions in the post_content
		$users = $this->get_users_from_mentions($mentions);

		// get all users that have already been notified of the mention
		$already_notified_ids = $this->get_notified_ids();

		// filter out users already notified
		$users_to_notify = array();
		foreach( $users as $user) {
			if (! in_array($user->ID, $already_notified_ids)) {
				$users_to_notify[] = $user;
			}
		}

		if ($this->post->post_type == 'reply') {
			$topic_link = get_permalink(bbp_get_reply_topic_id($this->post->ID)) . '#post-' . $this->post->ID;
		} else {
			$topic_link = get_permalink($this->post->ID) . '#post-' . $this->post->ID;
		}

		foreach ($users_to_notify as $notified_user) {
			error_log('Notify: ' . $notified_user->user_email);
			$email_result = $this->send_mention_notification($notified_user, array('link' => $topic_link));
			error_log('Email sent. Status: ' . $email_result ? 'Ok' : 'Failed');
			$already_notified_ids[] = $notified_user->ID;
		}

		$this->update_notified_ids($already_notified_ids);

		return true;
	}

	private function get_users_from_mentions($mentions_array) {
		$users = array();

		foreach ($mentions_array as $mention) {
			/*
			 * $mention is in /@[\w\d]+ / format
			 */
			$mention = substr($mention, 1);

			$user = get_user_by('slug', $mention);
			if ($user !== false) {
				$users[] = $user;
			}
		}

		return $users;
	}

	/**
	 * Returns the list of notified users for the current post id
	 * @return array
	 */
	public function get_notified_ids() {
		$notified = get_post_meta($this->post->ID, 'jm_notified_users', true);

		if ($notified == '') {
			$notified = array();
		}

		return $notified;
	}

	/**
	 * @param array $notified_ids
	 */
	public function update_notified_ids($notified_ids) {
		update_post_meta($this->post->ID, 'jm_notified_users', $notified_ids);
	}

	/**
	 * Gets all the mentions in the current post content
	 *
	 * @return bool|array
	 */
	private function get_mentions() {
		$subject = $this->post->post_content;
		$pattern = JMContentFilter::REGEX;
		preg_match_all($pattern, $subject, $matches);

		if ($matches === false) {
			return false;
		}

		if (! isset($matches[0])) {
			return false;
		}

		// $matches is an array of matched @mentions
		return array_unique($matches[0]);
	}

	/**
	 * @param WP_User $notified_user
	 *
	 * @return bool
	 */
	private function send_mention_notification($notified_user, $args = array()) {
		$subject = 'You\'ve Just Been Mentioned on Geoze.com';

		ob_start();
		include(JUST_MENTION_PATH . '/views/notify-email.php');
		$content = ob_get_contents();
		ob_end_clean();

		if (isset($args['link'])) {
			$content = str_replace('%%%LINK%%%', $args['link'], $content);
		}

		// Send email to admin.
		return wp_mail($notified_user->user_email, $subject, $content);
	}
}