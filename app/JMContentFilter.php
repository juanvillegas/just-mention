<?php

class JMContentFilter {

	const REGEX = '/@[A-Za-z0-9_-]+/';

	private $content;

	function __construct($content) {
		$this->content = $content;
	}

	public function filter() {
		$matches = array();

		preg_match_all( self::REGEX, $this->content, $matches );

		if (count( $matches[0] ) > 0) {
			$matches = array_unique( $matches[0] );

			foreach ($matches as $mention ) {
				$username = substr($mention, 1);

				$user = get_user_by('slug', $username);

				if ($user === false) {
					continue;
				}

				$this->content = str_replace($mention, $user->data->display_name, $this->content);
			}
		}

		return $this->content;
	}
}